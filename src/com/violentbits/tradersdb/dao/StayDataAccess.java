package com.violentbits.tradersdb.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Iterator;
import java.util.List;

import com.violentbits.dbutils.dao.DataAccess;
import com.violentbits.tradersdb.entities.Stay;

public class StayDataAccess {
	public static void insert(List<Stay> stays) throws SQLException {
		String sql="INSERT INTO Stays(StartDate, TraderId, EndDate, PlanetId) VALUES (?, ?, ?, ?)";
		PreparedStatement ps = DataAccess.getInstance().preparedStatement(sql, Statement.RETURN_GENERATED_KEYS);
		for (Stay stay : stays) {
			ps.setString(1, stay.startDate.toString());
			ps.setLong(2, stay.trader.id);
			ps.setString(3, stay.endDate==null?null:stay.endDate.toString());
			ps.setLong(4, stay.planet.id);
			ps.addBatch();
		}
		ps.executeBatch();
		
		ResultSet rs = ps.getGeneratedKeys();
		Iterator<Stay> it = stays.iterator();
		while (rs.next()) {
			it.next().id=rs.getLong(1);
		}
	}
}
