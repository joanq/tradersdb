package com.violentbits.tradersdb.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.violentbits.dbutils.dao.DataAccess;
import com.violentbits.tradersdb.entities.Product;

public class ProductDataAccess {
	public static Product getById(int id) throws SQLException {
		String sql = "SELECT * FROM Products WHERE id=?";
		PreparedStatement ps = DataAccess.getInstance().preparedStatement(sql);
		ps.setInt(1, id);
		Product product=null;
		
		try (ResultSet rs = ps.executeQuery()) {
			rs.next();
			product = new Product(rs.getInt("Id"), rs.getString("Name"));
			product.name = rs.getString("Name");
		}
		return product;
	}
	
	public static int getCount() throws SQLException {
		String sql = "SELECT COUNT(*) FROM Products";
		PreparedStatement ps = DataAccess.getInstance().preparedStatement(sql);
		int count=-1;
		try (ResultSet rs = ps.executeQuery()) {
			rs.next();
			count = rs.getInt(1);
		}
		return count;
	}
	
	public static List<Product> all() throws SQLException {
		Product product;
		List<Product> products = new ArrayList<>();
		String sql = "SELECT * FROM Products ORDER BY Id";
		PreparedStatement ps = DataAccess.getInstance().preparedStatement(sql);
		try (ResultSet rs = ps.executeQuery()) {
			while (rs.next()) {
				product = new Product(rs.getInt("Id"), rs.getString("Name"));
				product.name=rs.getString("Name");
				products.add(product);
			}
		}
		return products;
	}
	
	public static Product getRandom() throws SQLException {
		String sql = "SELECT * FROM Products ORDER BY RAND() LIMIT 1";
		PreparedStatement ps = DataAccess.getInstance().preparedStatement(sql);
		Product product=null;
		
		try (ResultSet rs = ps.executeQuery(sql)) {
			rs.next();
			product = new Product(rs.getInt("Id"), rs.getString("Name"));
		}
		return product;
	}
	
	public static void insert(List<Product> products) throws SQLException {
		String sql = "INSERT INTO Products(Name) VALUES (?)";
		PreparedStatement ps = DataAccess.getInstance().preparedStatement(sql);
		
		for (Product product : products) {
			ps.setString(1, product.name);
			ps.addBatch();
		}
		ps.executeBatch();
	}
}
