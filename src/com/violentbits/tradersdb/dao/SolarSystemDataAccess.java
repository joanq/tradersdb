package com.violentbits.tradersdb.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.violentbits.dbutils.dao.DataAccess;
import com.violentbits.tradersdb.entities.Planet;
import com.violentbits.tradersdb.entities.SolarSystem;

public class SolarSystemDataAccess {	
	public static SolarSystem getByCode(String code) throws SQLException {
		String sql = "SELECT * FROM SolarSystems WHERE Code=?";
		PreparedStatement ps = DataAccess.getInstance().preparedStatement(sql);
		ps.setString(1, code);
		SolarSystem system=null;
		
		try (ResultSet rs = ps.executeQuery()) {
			if (rs.next()) {
				system = new SolarSystem(rs.getInt("Id"), rs.getString("Code"), rs.getString("Name"));
			}
		}
		return system;
	}
	
	private static List<SolarSystem> executePreparedStatement(PreparedStatement ps) throws SQLException {
		List<SolarSystem> solarSystems = new ArrayList<>();
		try (ResultSet rs = ps.executeQuery()) {
			while (rs.next()) {
				solarSystems.add(new SolarSystem(
						rs.getLong("Id"),
						rs.getString("Code"),
						rs.getString("Name")));
			}
		}
		return solarSystems;
	}
	
	public static List<SolarSystem> all() throws SQLException {
		String sql = "SELECT * FROM SolarSystems ORDER BY Id";
		PreparedStatement ps = DataAccess.getInstance().preparedStatement(sql);
		return executePreparedStatement(ps);
	}	
	
	public static SolarSystem getByPlanet(Planet planet) throws SQLException {
		String sql = "SELECT s.* FROM SolarSystems s "
				+ "JOIN Planets p ON s.Id=p.SystemId "
				+ "WHERE p.Id=?";
		PreparedStatement ps = DataAccess.getInstance().preparedStatement(sql);
		ps.setLong(1, planet.id);
		SolarSystem system=null;
		
		try (ResultSet rs = ps.executeQuery()) {
			rs.next();
			system = new SolarSystem(rs.getInt("s.Id"), rs.getString("s.Code"), rs.getString("s.Name"));
		}
		return system;
	}
	
	public static void insert(List<SolarSystem> systems) throws SQLException {
		String sql = "INSERT INTO SolarSystems(Code, Name) VALUES (?, ?)";
		PreparedStatement ps = DataAccess.getInstance().preparedStatement(sql);
		
		for (SolarSystem system : systems) {
			ps.setString(1, system.code);
			ps.setString(2, system.name);
			ps.addBatch();
		}
		ps.executeBatch();
	}
}
