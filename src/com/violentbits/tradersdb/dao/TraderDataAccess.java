package com.violentbits.tradersdb.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.violentbits.dbutils.dao.DataAccess;
import com.violentbits.tradersdb.entities.Trader;

public class TraderDataAccess {
	public static List<Trader> all() throws SQLException {
		List<Trader> traders = new ArrayList<>();
		Trader trader;
		String sql = "SELECT * FROM Traders ORDER BY Id";
		try (ResultSet rs = DataAccess.getInstance().executeQuery(sql)) {
			while (rs.next()) {
				trader = new Trader();
				trader.id = rs.getInt("Id");
				trader.nickname = rs.getString("Nickname");
				trader.firstname = rs.getString("Firstname");
				trader.lastname = rs.getString("Lastname");
				if (rs.getString("DeceaseDate")!=null)
					trader.deceaseDate=LocalDate.parse(rs.getString("DeceaseDate"));
				traders.add(trader);
			}
		}
		return traders;
	}
	
	public static void update(Trader trader) throws SQLException {
		String sql="UPDATE Traders SET DeceaseDate=? WHERE Id=?";
		PreparedStatement ps = DataAccess.getInstance().preparedStatement(sql);
		ps.setString(1, trader.deceaseDate==null?null:trader.deceaseDate.toString());
		ps.setLong(2, trader.id);
		ps.executeQuery();
	}
	
	public static void insert(List<Trader> traders) throws SQLException {
		String sql = "INSERT INTO Traders(Nickname, Firstname, Lastname, DeceaseDate) VALUES (?, ?, ?, ?)";
		PreparedStatement ps = DataAccess.getInstance().preparedStatement(sql);
		
		for (Trader trader : traders) {
			ps.setString(1, trader.nickname);
			ps.setString(2, trader.firstname);
			ps.setString(3, trader.lastname);
			ps.setString(4, trader.deceaseDate==null?null:trader.deceaseDate.toString());
			ps.addBatch();
		}
		ps.executeBatch();
	}
}
