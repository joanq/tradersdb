package com.violentbits.tradersdb.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.violentbits.dbutils.dao.DataAccess;
import com.violentbits.tradersdb.entities.Planet;
import com.violentbits.tradersdb.entities.SolarSystem;

public class PlanetDataAccess {
	public static Planet getById(int id) throws SQLException {
		String sql = String.format("SELECT * FROM Planets WHERE id=?");
		PreparedStatement ps = DataAccess.getInstance().preparedStatement(sql);
		ps.setInt(1, id);
		Planet planet=null;
		
		try (ResultSet rs = ps.executeQuery()) {
			rs.next();
			planet = new Planet(rs.getInt("id"));
		}
		return planet;
	}
	
	public static int getCount() throws SQLException {
		String sql = "SELECT COUNT(*) FROM Planets";
		PreparedStatement ps = DataAccess.getInstance().preparedStatement(sql);
		int count=-1;
		try (ResultSet rs = ps.executeQuery(sql)) {
			rs.next();
			count = rs.getInt(1);
		}
		return count;
	}
	
	public static List<Planet> getBySystem(SolarSystem system) throws SQLException {
		List<Planet> planets = new ArrayList<>();
		String sql = "SELECT * FROM Planets p "
				+ "JOIN SolarSystems s ON p.SystemId=s.Id "
				+ "WHERE s.Id = ?";
		PreparedStatement ps = DataAccess.getInstance().preparedStatement(sql);
		ps.setLong(1, system.getId());
		try (ResultSet rs = ps.executeQuery()) {
			while (rs.next()) {
				planets.add(new Planet(rs.getInt("id")));
			}
		}
		return planets;
	}
	
	public static Planet getRandom() throws SQLException {
		String sql = "SELECT * FROM Planets ORDER BY RAND() LIMIT 1";
		PreparedStatement ps = DataAccess.getInstance().preparedStatement(sql);
		Planet planet=null;
		
		try (ResultSet rs = ps.executeQuery()) {
			rs.next();
			planet = new Planet(rs.getInt("id"));
		}
		return planet;
	}
	
	public static void save(Planet planet) throws SQLException {
		String sql;
		if (planet.id==0) { // New
			sql=String.format("INSERT INTO Planets(Code, Name, Position, SystemId, AverageDistance) "
					+ "VALUES (\"%s\", \"%s\", %d, %d, %.2f)",
					planet.code, planet.name, planet.position,
					planet.system.getId(), planet.averageDistance);
			planet.id = DataAccess.getInstance().executeInsert(sql);
		} else { // Update
			sql=String.format("UPDATE Planets SET Code='%s', Name='%s', Position=%d, "
					+ "SystemId=%d, AverageDistance=%.2f "
					+ "WHERE Id=%d",
					planet.code, planet.name, planet.position,
					planet.system.getId(), planet.averageDistance,
					planet.id);
			DataAccess.getInstance().executeUpdate(sql);
		}
	}
	
	public static void insert(List<Planet> planets) throws SQLException {
		String sql = "INSERT INTO Planets(Code, Name, Position, SystemId, AverageDistance) VALUES (?, ?, ?, ?, ?)";
		PreparedStatement ps = DataAccess.getInstance().preparedStatement(sql);
		
		for (Planet planet : planets) {
			ps.setString(1, planet.code);
			ps.setString(2, planet.name);
			ps.setInt(3, planet.position);
			ps.setLong(4, planet.system.getId());
			ps.setDouble(5, planet.averageDistance);
			ps.addBatch();
		}
		ps.executeBatch();
	}
}
