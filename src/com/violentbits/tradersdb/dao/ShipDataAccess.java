package com.violentbits.tradersdb.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.violentbits.dbutils.dao.DataAccess;
import com.violentbits.tradersdb.entities.Ship;
import com.violentbits.tradersdb.entities.Trader;
import com.violentbits.tradersdb.traders.Traders;

public class ShipDataAccess {
	public static Ship getById(int id) throws SQLException {
		String sql = String.format("SELECT * FROM Ships WHERE id=%d", id);
		Ship ship=null;
		
		try (ResultSet rs = DataAccess.getInstance().executeQuery(sql)) {
			rs.next();
			ship = new Ship(
					rs.getLong("Id"),
					rs.getString("Name"),
					Traders.shipTypeGenerator.byName(rs.getString("Type")),
					rs.getInt("Capacity"));
		}
		return ship;
	}
	
	public static int getCount() throws SQLException {
		String sql = "SELECT COUNT(*) FROM Ships";
		int count=-1;
		try (ResultSet rs = DataAccess.getInstance().executeQuery(sql)) {
			rs.next();
			count = rs.getInt(1);
		}
		return count;
	}
	
	private static List<Ship> executeQuery(String sql) throws SQLException {
		List<Ship> ships = new ArrayList<>();
		try (ResultSet rs = DataAccess.getInstance().executeQuery(sql)) {
			while (rs.next()) {
				ships.add(new Ship(
						rs.getLong("Id"),
						rs.getString("Name"),
						Traders.shipTypeGenerator.byName(rs.getString("type")),
						rs.getInt("Capacity")));
			}
		}
		return ships;
	}
	
	private static List<Ship> executePreparedStatement(PreparedStatement ps) throws SQLException {
		List<Ship> ships = new ArrayList<>();
		try (ResultSet rs = ps.executeQuery()) {
			while (rs.next()) {
				ships.add(new Ship(
						rs.getLong("Id"),
						rs.getString("Name"),
						Traders.shipTypeGenerator.byName(rs.getString("type")),
						rs.getInt("Capacity")));
			}
		}
		return ships;
	}
	
	public static List<Ship> all() throws SQLException {
		String sql = "SELECT * FROM Ships ORDER BY Id";
		return executeQuery(sql);
	}
	
	public static List<Ship> byType(String type) throws SQLException {
		String sql = String.format("SELECT * FROM Ships WHERE type LIKE '%s' ORDER BY Id", type);
		return executeQuery(sql);
	}
	
	public static List<Ship> ownedBy(Trader trader, LocalDate date) throws SQLException {
		String sql = "SELECT * FROM Ships s JOIN ShipOwners so ON s.Id=so.ShipId "
				+ "WHERE TraderId=? AND AcquisitionDate<? AND (LostDate IS NULL OR LostDate>?) "
				+ "ORDER BY s.Id";
		PreparedStatement ps = DataAccess.getInstance().preparedStatement(sql);
		ps.setLong(1, trader.id);
		ps.setString(2, date.toString());
		ps.setString(3, date.toString());
		return executePreparedStatement(ps);
	}
	
	public static List<Ship> available(LocalDate date) throws SQLException {
		String sql = "SELECT * FROM Ships s "
				+ "WHERE NOT EXISTS ("
				+ "  SELECT * FROM ShipOwners"
				+ "  WHERE ShipId=s.Id "
				+ "  AND AcquisitionDate<? "
				+ "  AND (LostDate IS NULL OR LostDate>?) "
				+ "  AND LostCause NOT LIKE 'Destroyed%%'"
				+ ") ORDER BY Id";
		PreparedStatement ps = DataAccess.getInstance().preparedStatement(sql);
		ps.setString(1, date.toString());
		ps.setString(2, date.toString());
		return executePreparedStatement(ps);
	}
	
	public static void save(Ship ship) throws SQLException {
		String sql;
		if (ship.getId()==0) { // New
			sql=String.format("INSERT INTO Ships(Name, Type, Capacity) "
					+ "VALUES (\"%s\", \"%s\", %d)",
					ship.getName(), ship.getShipType().name, ship.capacity);
			ship.setId(DataAccess.getInstance().executeInsert(sql));
		} else { // Update
			sql=String.format("UPDATE Ships SET Name=\"%s\", Capacity=%d"
					+ "WHERE Id=%d",
					ship.getName(), ship.capacity, ship.getId());
			DataAccess.getInstance().executeUpdate(sql);
		}
	}
	
	public static void insert(List<Ship> ships) throws SQLException {
		String sql = "INSERT INTO Ships(Name, Type, Capacity) VALUES (?, ?, ?)";
		PreparedStatement ps = DataAccess.getInstance().preparedStatement(sql);
		
		for (Ship ship : ships) {
			ps.setString(1, ship.getName());
			ps.setString(2, ship.getShipType().name);
			ps.setInt(3, ship.capacity);
			ps.addBatch();
		}
		ps.executeBatch();
	}
}
