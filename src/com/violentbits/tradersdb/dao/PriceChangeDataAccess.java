package com.violentbits.tradersdb.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

import com.violentbits.dbutils.dao.DataAccess;
import com.violentbits.tradersdb.entities.Planet;
import com.violentbits.tradersdb.entities.PriceChange;
import com.violentbits.tradersdb.entities.Product;

public class PriceChangeDataAccess {
	public static PriceChange getLastChange(Product product, Planet planet, LocalDate date) throws SQLException {
		String sql = "SELECT * FROM PriceChanges WHERE ProductId=? AND PlanetId=? AND Date<? ORDER BY Date DESC LIMIT 1";
		PreparedStatement ps = DataAccess.getInstance().preparedStatement(sql);
		ps.setLong(1, product.id);
		ps.setLong(2, planet.id);
		ps.setString(3, date.toString());
		PriceChange priceChange=null;
		try (ResultSet rs = ps.executeQuery()) {
			if (rs.next()) {
				priceChange = new PriceChange();
				priceChange.id = rs.getInt("Id");
				priceChange.date = LocalDate.parse(rs.getString("Date"));
				priceChange.product = product;
				priceChange.planet = planet;
				priceChange.newPrice = rs.getDouble("NewPrice");
			}
		}
		return priceChange;
	}
	
	public static PriceChange getBestPrice(Product product, LocalDate date) throws SQLException {
		String sql = "SELECT * FROM PriceChanges pc WHERE ProductId = ? AND Date = ("
				+ "  SELECT MAX(Date) FROM PriceChanges WHERE ProductId = ? AND PlanetId = pc.PlanetId "
				+ "  AND Date < ? GROUP BY planetId ) ORDER BY NewPrice LIMIT 1";
		PreparedStatement ps = DataAccess.getInstance().preparedStatement(sql);
		ps.setLong(1, product.id);
		ps.setLong(2, product.id);
		ps.setString(3, date.toString());
		PriceChange priceChange=null;
		try (ResultSet rs = ps.executeQuery()) {
			if (rs.next()) {
				priceChange = new PriceChange();
				priceChange.id = rs.getInt("Id");
				priceChange.date = LocalDate.parse(rs.getString("Date"));
				priceChange.product = product;
				priceChange.planet = PlanetDataAccess.getById(rs.getInt("PlanetId"));
				priceChange.newPrice = rs.getDouble("NewPrice");
			}
		}
		return priceChange;
	}
	
	public static double averagePrice(Product product, LocalDate date) throws SQLException {
		double price=0;
		String sql = "SELECT AVG(NewPrice) FROM PriceChanges pc WHERE (ProductId, PlanetId, Date) IN ("
				+ "  SELECT ProductId, PlanetId, MAX(Date) FROM PriceChanges "
				+ "  WHERE ProductId = ? AND Date < ? GROUP BY PlanetId ) ";
		PreparedStatement ps = DataAccess.getInstance().preparedStatement(sql);
		ps.setLong(1, product.id);
		ps.setString(2, date.toString());
		try (ResultSet rs = ps.executeQuery()) {
			rs.next();
			price=rs.getDouble(1);
		}
		return price;
	}
	
	public static double maxPrice(Product product, LocalDate date) throws SQLException {
		double price=0;
		String sql = "SELECT MAX(NewPrice) FROM PriceChanges pc WHERE (ProductId, PlanetId, Date) IN ("
				+ "  SELECT ProductId, PlanetId, MAX(Date) FROM PriceChanges "
				+ "  WHERE ProductId = ? AND Date < ? GROUP BY PlanetId ) ";
		PreparedStatement ps = DataAccess.getInstance().preparedStatement(sql);
		ps.setLong(1, product.id);
		ps.setString(2, date.toString());
		try (ResultSet rs = ps.executeQuery()) {
			rs.next();
			price=rs.getDouble(1);
		}
		return price;
	}
	
	public static void save(PriceChange priceChange) throws SQLException {
		String sql;
		if (priceChange.id==0) { // New
			sql=String.format("INSERT INTO PriceChanges(Date, ProductId, PlanetId, NewPrice) "
					+ "VALUES (\"%s\", %d, %d, %.2f)",
					priceChange.date, priceChange.product.id, priceChange.planet.id, priceChange.newPrice);
			priceChange.id = DataAccess.getInstance().executeInsert(sql);
		} else { // Update
			sql=String.format("UPDATE PriceChange SET Date=\"%s\", ProductId=%d, PlanetId=%d, NewPrice=%.2f "
					+ "WHERE Id=%d",
					priceChange.date, priceChange.product.id, priceChange.planet.id, priceChange.newPrice);
			DataAccess.getInstance().executeUpdate(sql);
		}
	}
	
	public static void insert(List<PriceChange> priceChanges) throws SQLException {
		String sql = "INSERT INTO PriceChanges(Date, ProductId, PlanetId, NewPrice) VALUES (?, ?, ?, ?)";
		PreparedStatement ps = DataAccess.getInstance().preparedStatement(sql);
		
		for (PriceChange priceChange : priceChanges) {
			ps.setString(1, priceChange.date.toString());
			ps.setLong(2, priceChange.product.id);
			ps.setLong(3, priceChange.planet.id);
			ps.setDouble(4, priceChange.newPrice);
			ps.addBatch();
		}
		ps.executeBatch();
	}
}
