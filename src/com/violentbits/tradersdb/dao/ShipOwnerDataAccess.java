package com.violentbits.tradersdb.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

import com.violentbits.dbutils.dao.DataAccess;
import com.violentbits.tradersdb.entities.Ship;
import com.violentbits.tradersdb.entities.ShipOwner;
import com.violentbits.tradersdb.entities.Trader;

public class ShipOwnerDataAccess {
	public static ShipOwner byShipTraderDate(Ship ship, Trader trader, LocalDate date) throws SQLException {
		String sql = "SELECT * FROM ShipOwners "
				+ "WHERE TraderId=? "
				+ "AND ShipId=? "
				+ "AND AcquisitionDate<? "
				+ "AND (LostDate IS NULL OR LostDate>?)";
		PreparedStatement ps = DataAccess.getInstance().preparedStatement(sql);
		ps.setLong(1, trader.id);
		ps.setLong(2, ship.getId());
		ps.setString(3, date.toString());
		ps.setString(4, date.toString());
		ShipOwner shipOwner=null;
		
		try (ResultSet rs = ps.executeQuery()) {
			if (rs.next()) {
				shipOwner = new ShipOwner(ship, trader,
						LocalDate.parse(rs.getString("AcquisitionDate")),
						rs.getString("AcquisitionCause"));
				shipOwner.id = rs.getInt("Id");
				shipOwner.lostDate = rs.getString("LostDate")==null?null:LocalDate.parse(rs.getString("LostDate"));
				shipOwner.lostCause = rs.getString("LostCause");
				shipOwner.lostBenefit = rs.getDouble("LostBenefit");
			}
		}
		return shipOwner;
	}
	
	public static void update(ShipOwner shipOwner) throws SQLException {
		String sql="UPDATE ShipOwners SET LostDate=?, LostBenefit=?, LostCause=? WHERE Id=?";
		PreparedStatement ps = DataAccess.getInstance().preparedStatement(sql);
		ps.setString(1, shipOwner.lostDate==null?null:shipOwner.lostDate.toString());
		ps.setDouble(2, shipOwner.lostBenefit);
		ps.setString(3, shipOwner.lostCause);
		ps.setLong(4, shipOwner.id);
		ps.executeQuery();
	}
	
	public static void insert(ShipOwner shipOwner) throws SQLException {
		String sql="INSERT INTO ShipOwners(ShipId, TraderId, AcquisitionDate, "
				+ "AcquisitionCause, AcquisitionPrice, LostDate, LostBenefit, LostCause) "
				+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
		PreparedStatement ps = DataAccess.getInstance().preparedStatement(sql);
		ps.setLong(1, shipOwner.ship.getId());
		ps.setLong(2, shipOwner.trader.id);
		ps.setString(3, shipOwner.acquisitionDate.toString());
		ps.setString(4, shipOwner.acquisitionCause);
		ps.setDouble(5, shipOwner.acquisitionPrice);
		ps.setString(6, shipOwner.lostDate==null?null:shipOwner.lostDate.toString());
		ps.setDouble(7, shipOwner.lostBenefit);
		ps.setString(8, shipOwner.lostCause);
		ps.executeQuery();
	}
	
	public static void insert(List<ShipOwner> shipOwners) throws SQLException {
		String sql="INSERT INTO ShipOwners(ShipId, TraderId, AcquisitionDate, "
				+ "AcquisitionCause, AcquisitionPrice, LostDate, LostBenefit, LostCause) "
				+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
		PreparedStatement ps = DataAccess.getInstance().preparedStatement(sql);
		for (ShipOwner shipOwner : shipOwners) {
			ps.setLong(1, shipOwner.ship.getId());
			ps.setLong(2, shipOwner.trader.id);
			ps.setString(3, shipOwner.acquisitionDate.toString());
			ps.setString(4, shipOwner.acquisitionCause);
			ps.setDouble(5, shipOwner.acquisitionPrice);
			ps.setString(6, shipOwner.lostDate==null?null:shipOwner.lostDate.toString());
			ps.setDouble(7, shipOwner.lostBenefit);
			ps.setString(8, shipOwner.lostCause);
			ps.addBatch();
		}
		ps.executeBatch();
	}
}
