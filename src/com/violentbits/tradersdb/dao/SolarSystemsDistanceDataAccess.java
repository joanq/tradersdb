package com.violentbits.tradersdb.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.violentbits.dbutils.dao.DataAccess;
import com.violentbits.tradersdb.entities.SolarSystem;
import com.violentbits.tradersdb.entities.SolarSystemsDistance;

public class SolarSystemsDistanceDataAccess {
	public static double distance(SolarSystem from, SolarSystem to) throws SQLException {
		String sql = "SELECT Distance FROM SolarSystemsDistances "
				+ "WHERE (SystemId1=? AND SystemId2=?) OR (SystemId2=? AND SystemId1=?)";
		PreparedStatement ps = DataAccess.getInstance().preparedStatement(sql);
		ps.setLong(1, from.getId());
		ps.setLong(2, to.getId());
		ps.setLong(3, from.getId());
		ps.setLong(4, to.getId());
		double distance=0;
		
		try (ResultSet rs = ps.executeQuery()) {
			if (rs.next())
				distance = rs.getDouble("Distance");
		}
		return distance;
	}
	
	public static void insert(List<SolarSystemsDistance> distances) throws SQLException {
		String sql = "INSERT INTO SolarSystemsDistances(SystemId1, SystemId2, Distance) VALUES (?, ?, ?)";
		PreparedStatement ps = DataAccess.getInstance().preparedStatement(sql);
		
		for (SolarSystemsDistance distance : distances) {
			ps.setLong(1, distance.system1.getId());
			ps.setLong(2, distance.system2.getId());
			ps.setDouble(3, distance.distance);
			ps.addBatch();
		}
		ps.executeBatch();
	}
}
