package com.violentbits.tradersdb.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Iterator;
import java.util.List;

import com.violentbits.dbutils.dao.DataAccess;
import com.violentbits.tradersdb.entities.Trade;
import com.violentbits.tradersdb.entities.TradeLine;

public class TradeDataAccess {
	public static void insert(List<Trade> trades) throws SQLException {
		String sql = "INSERT INTO Trades(StayId, DateTime, Type, Total) VALUES (?, ?, ?, ?)";
		PreparedStatement ps = DataAccess.getInstance().preparedStatement(sql, Statement.RETURN_GENERATED_KEYS);
		for (Trade trade : trades) {
			ps.setLong(1, trade.stay.id);
			ps.setString(2, trade.dateTime.toString());
			ps.setString(3, trade.type.toString());
			ps.setDouble(4, trade.total);
			ps.addBatch();
		}
		ps.executeBatch();
		
		ResultSet rs = ps.getGeneratedKeys();
		Iterator<Trade> it = trades.iterator();
		while (rs.next()) {
			it.next().id=rs.getLong(1);
		}
		
		sql="INSERT INTO TradeLines(ProductId, TradeId, UnitPrice, Quantity) VALUES (?, ?, ?, ?)";
		ps = DataAccess.getInstance().preparedStatement(sql);
		for (Trade trade : trades) {
			for (TradeLine line : trade.lines) {
				ps.setLong(1, line.product.id);
				ps.setLong(2, trade.id);
				ps.setDouble(3, line.unitPrice);
				ps.setInt(4, line.quantity);
				ps.addBatch();
			}
		}
		ps.executeBatch();
	}
}
