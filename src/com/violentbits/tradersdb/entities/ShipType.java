package com.violentbits.tradersdb.entities;

import com.violentbits.dbutils.storage.NormalDataStorage;

public class ShipType {
	public final String name;
	public final int baseCapacity;
	public final int basePrice;
	public final String[] prefixes;
	private NormalDataStorage capacityDataStorage;
	
	public ShipType(String name, int baseCapacity, int basePrice, String[] prefixes) {
		this.name = name;
		this.baseCapacity = baseCapacity;
		this.basePrice = basePrice;
		this.prefixes = prefixes;
		capacityDataStorage = new NormalDataStorage(baseCapacity, baseCapacity*0.5, 0, baseCapacity*10);
	}
	
	public int getRandomCapacity() {
		return (int)Math.round(capacityDataStorage.get());
	}
}
