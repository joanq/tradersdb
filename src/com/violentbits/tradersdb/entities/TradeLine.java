package com.violentbits.tradersdb.entities;

public class TradeLine {
	public long id;
	public Product product;
	public double unitPrice;
	public int quantity;
}
