package com.violentbits.tradersdb.entities;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Trade {
	public enum Type {Buy, Sell};
	public long id;
	public Stay stay;
	public LocalDateTime dateTime;
	public Type type;
	public double total;
	public List<TradeLine> lines = new ArrayList<>();
}
