package com.violentbits.tradersdb.entities;

import java.time.LocalDate;

public class Trader {
	public long id;
	public String nickname;
	public String firstname;
	public String lastname;
	public LocalDate deceaseDate;
	
	public LocalDate startingDate;
	public LocalDate endingDate;
	
	public Trader() {}
	
	public Trader(String nickname, String firstname, String lastname) {
		this.nickname = nickname;
		this.firstname = firstname;
		this.lastname = lastname;
	}
}
