package com.violentbits.tradersdb.entities;

public class SolarSystemsDistance {
	public SolarSystem system1;
	public SolarSystem system2;
	public double distance;
	
	public SolarSystemsDistance(SolarSystem system1, SolarSystem system2, double distance) {
		this.system1 = system1;
		this.system2 = system2;
		this.distance = distance;
	}
}
