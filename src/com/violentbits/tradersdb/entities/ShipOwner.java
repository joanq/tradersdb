package com.violentbits.tradersdb.entities;

import java.time.LocalDate;

public class ShipOwner {
	public static final String[] acquisitionCauses = {
		"Bought", "Bought", "Inherited", "Stolen"	
	};
	public static final String[] lostCauses = {
		"Sold", "Sold", "Destroyed", "Stolen"	
	};
	public long id;
	public Ship ship;
	public Trader trader;
	public LocalDate acquisitionDate;
	public String acquisitionCause;
	public double acquisitionPrice;
	public LocalDate lostDate;
	public double lostBenefit;
	public String lostCause="";
	
	public ShipOwner(Ship ship, Trader trader, LocalDate acquisitionDate, String acquisitionCause) {
		this.ship=ship;
		this.trader=trader;
		this.acquisitionDate=acquisitionDate;
		this.acquisitionCause=acquisitionCause;
	}
}
