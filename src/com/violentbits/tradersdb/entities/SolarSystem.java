package com.violentbits.tradersdb.entities;

import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import com.violentbits.tradersdb.dao.PlanetDataAccess;

public class SolarSystem {
	private long id;
	public String code;
	public String name;
	private List<Planet> planets;
	public final double x, y, z;
	
	public SolarSystem(long id, String code, String name) {
		this(code, name);
		this.id = id;
	}
	
	public SolarSystem(String code, String name) {
		this.code = code;
		this.name = name;
		x = ThreadLocalRandom.current().nextDouble(-5, 6);
		y = ThreadLocalRandom.current().nextDouble(-5, 6);
		z = ThreadLocalRandom.current().nextDouble(-5, 6);
	}
	
	public double distance(SolarSystem s) {
		return Math.sqrt((x-s.x)*(x-s.x)+(y-s.y)*(y-s.y)+(z-s.z)*(z-s.z));
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public List<Planet> getPlanets() throws SQLException {
		if (planets == null) {
			planets = PlanetDataAccess.getBySystem(this);
		}
		return planets;
	}
}
