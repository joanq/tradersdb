package com.violentbits.tradersdb.entities;

public class Planet {
	public long id;
	public String code;
	public String name;
	public int position;
	public SolarSystem system;
	public double averageDistance;
	
	public Planet(long id) {
		this.id = id;
	}
	
	private String generateCode() {
		return system.code+Character.toUpperCase(name.charAt(0))+Character.toUpperCase(name.charAt(1))+
				String.format("%02d", position);
	}
	
	public Planet(String name, int position, SolarSystem system, double averageDistance) {
		this.name = name;
		this.position = position;
		this.system = system;
		this.averageDistance = averageDistance;
		code = generateCode();
	}
}
