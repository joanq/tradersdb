package com.violentbits.tradersdb.entities;

import java.time.LocalDate;

public class PriceChange {
	public long id;
	public LocalDate date;
	public Product product;
	public Planet planet;
	public double newPrice;
}
