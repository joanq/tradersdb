package com.violentbits.tradersdb.entities;

import java.util.concurrent.ThreadLocalRandom;

public class Product {
	public long id;
	public String name;
	public final double basePrice = ThreadLocalRandom.current().nextDouble(100, 10000);
	
	public Product(long id, String name) {
		this.id = id;
		this.name = name;
	}
	
	public Product(String name) {
		this.name = name;
	}
}
