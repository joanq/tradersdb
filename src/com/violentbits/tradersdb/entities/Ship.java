package com.violentbits.tradersdb.entities;

public class Ship {
	private long id;
	private String name;
	public final int capacity;
	private ShipType shipType;
	
	public Ship(long id, String name, ShipType shipType, int capacity) {
		this.id = id;
		this.shipType = shipType;
		this.capacity = capacity;
		this.name = name;
	}
	
	public Ship(String name, ShipType shipType, int capacity) {
		this.name = name;
		this.shipType = shipType;
		this.capacity = capacity;
	}
	
	public double getBasePrice() {
		return shipType.basePrice*capacity;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public long getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public ShipType getShipType() {
		return shipType;
	}
}
