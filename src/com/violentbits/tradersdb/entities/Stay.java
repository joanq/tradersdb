package com.violentbits.tradersdb.entities;

import java.time.LocalDate;

public class Stay {
	public long id;
	public LocalDate startDate;
	public Trader trader;
	public LocalDate endDate;
	public Planet planet;
}
