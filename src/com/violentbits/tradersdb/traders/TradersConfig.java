package com.violentbits.tradersdb.traders;

import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Properties;

public class TradersConfig {
	private static final String propertiesFile = "config.properties";
	public static TradersConfig current = new TradersConfig();
	public final String db;
	public final String dbhostname;
	public final String dbusername;
	public final String dbpassword;
	public final boolean debug;
	public final String shipTypesFile;
	public final String shipsFile;
	public final String firstnamesFile;
	public final String lastnamesFile;
	public final String nicknamesFile;
	public final String solarsystemsFile;
	public final String planetsFile;
	public final String productsFile;
	public final LocalDate startingDate;
	public final LocalDate endingDate;
	public final int nShips;
	public final int nTraders;
	public final int nSolarSystems;
	public final int nProducts;
	public final double avgPlanetsPerSystem;
	public final double stddevPlanetsPerSystem;
	public final double minPlanetsPerSystem;
	public final double maxPlanetsPerSystem;
	public final double tradeInPlanetProbability;
	public final int maxStayDuration;
	public final double shipLossProbability;
	public final double shipBuyProbability;
	public final double traderDieProbability;
	public final int minDaysWithoutPriceChanges;
	public final int maxDaysWithoutPriceChanges;
	public final int minProductsPerPlanet;
	public final int maxProductsPerPlanet;

	private TradersConfig() {
		Properties properties = new Properties();
		try {
			properties.load(new FileReader(propertiesFile));
		} catch (IOException e) {
			System.err.println("Cannot read properties file: "+e.getMessage());
			System.exit(1);
		}
		db = properties.getProperty("db", "traders");
		dbhostname = properties.getProperty("dbhostname", "localhost");
		dbusername = properties.getProperty("dbusername", "user");
		dbpassword = properties.getProperty("dbpassword", "12345");
		debug = properties.getProperty("debug", "false").equals("true")?true:false;
		shipTypesFile = properties.getProperty("ship_types_file", "ship_types.csv");
		shipsFile = properties.getProperty("ships_file", "ships.csv");
		firstnamesFile = properties.getProperty("firstnames_file", "firstnames.csv");
		lastnamesFile = properties.getProperty("lastnames_file", "lastnames.csv");
		nicknamesFile = properties.getProperty("nicknames_file", "nicknames.csv");
		solarsystemsFile = properties.getProperty("solarsystems_file", "solarsystems.csv");
		planetsFile = properties.getProperty("planets_file", "planets.csv");
		productsFile = properties.getProperty("products_file", "products.csv");
		startingDate = LocalDate.parse(properties.getProperty("starting_date", "2200-01-01"));
		endingDate = LocalDate.parse(properties.getProperty("ending_date", "2500-01-01"));
		nShips = Integer.parseInt(properties.getProperty("n_ships", "100"));
		nTraders = Integer.parseInt(properties.getProperty("n_traders", "20"));
		nSolarSystems = Integer.parseInt(properties.getProperty("n_solar_systems", "10"));
		nProducts = Integer.parseInt(properties.getProperty("n_products", "30"));
		avgPlanetsPerSystem = Double.parseDouble(properties.getProperty("avg_planets_per_system", "5"));
		stddevPlanetsPerSystem = Double.parseDouble(properties.getProperty("stddev_planets_per_system", "3"));
		minPlanetsPerSystem = Double.parseDouble(properties.getProperty("min_planets_per_system", "1"));
		maxPlanetsPerSystem = Double.parseDouble(properties.getProperty("max_planets_per_system", "10"));
		tradeInPlanetProbability = Double.parseDouble(properties.getProperty("trade_in_planet_probability", "0.3"));
		maxStayDuration = Integer.parseInt(properties.getProperty("max_stay_duration", "1095"));
		shipLossProbability = Double.parseDouble(properties.getProperty("ship_loss_probability", "0.3"));
		shipBuyProbability = Double.parseDouble(properties.getProperty("ship_buy_probability", "0.4"));
		traderDieProbability = Double.parseDouble(properties.getProperty("trader_die_probability", "0.3"));
		minDaysWithoutPriceChanges = Integer.parseInt(properties.getProperty("min_days_without_price_changes", "0"));
		maxDaysWithoutPriceChanges = Integer.parseInt(properties.getProperty("max_days_without_price_changes", "100"));
		minProductsPerPlanet = Integer.parseInt(properties.getProperty("min_products_per_planet", "3"));
		maxProductsPerPlanet = Integer.parseInt(properties.getProperty("max_products_per_planet", "8"));
	}
}
