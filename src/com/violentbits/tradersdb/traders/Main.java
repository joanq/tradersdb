package com.violentbits.tradersdb.traders;

import static java.util.concurrent.ThreadLocalRandom.current;

import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;

import com.violentbits.dbutils.dao.DataAccess;
import com.violentbits.tradersdb.dao.PlanetDataAccess;
import com.violentbits.tradersdb.dao.PriceChangeDataAccess;
import com.violentbits.tradersdb.dao.ProductDataAccess;
import com.violentbits.tradersdb.dao.ShipDataAccess;
import com.violentbits.tradersdb.dao.ShipOwnerDataAccess;
import com.violentbits.tradersdb.dao.SolarSystemDataAccess;
import com.violentbits.tradersdb.dao.SolarSystemsDistanceDataAccess;
import com.violentbits.tradersdb.dao.StayDataAccess;
import com.violentbits.tradersdb.dao.TradeDataAccess;
import com.violentbits.tradersdb.dao.TraderDataAccess;
import com.violentbits.tradersdb.entities.Planet;
import com.violentbits.tradersdb.entities.PriceChange;
import com.violentbits.tradersdb.entities.Product;
import com.violentbits.tradersdb.entities.Ship;
import com.violentbits.tradersdb.entities.ShipOwner;
import com.violentbits.tradersdb.entities.SolarSystem;
import com.violentbits.tradersdb.entities.SolarSystemsDistance;
import com.violentbits.tradersdb.entities.Stay;
import com.violentbits.tradersdb.entities.Trade;
import com.violentbits.tradersdb.entities.TradeLine;
import com.violentbits.tradersdb.entities.Trader;

public class Main {	
	public static void main(String[] args) throws SQLException, IOException {
		Locale.setDefault(Locale.US);
		Main main = new Main();
		Traders.init();
		DataAccess.init(
				String.format("jdbc:mariadb://%s/%s", TradersConfig.current.dbhostname,	TradersConfig.current.db),
				TradersConfig.current.dbusername, TradersConfig.current.dbpassword);
		System.out.println("Start...");
		main.createStory();
		DataAccess.getInstance().close();
		System.out.println("End");
	}
	
	public void createShips() throws SQLException {
		List<Ship> ships = new ArrayList<>();
		for (int nShips=0; nShips<TradersConfig.current.nShips; nShips++) {
			ships.add(Traders.shipGenerator.getShip());
		}
		ShipDataAccess.insert(ships);
	}
	
	public void createTraders() throws SQLException {
		List<Trader> traders = new ArrayList<>();
		for (int nTraders=0; nTraders<TradersConfig.current.nTraders; nTraders++) {
			traders.add(Traders.traderGenerator.getTrader());
		}
		TraderDataAccess.insert(traders);
	}
	
	public void createSolarSystems() throws SQLException {
		List<SolarSystem> systems = new ArrayList<>();
		for (int nSolarSystems=0; nSolarSystems<TradersConfig.current.nSolarSystems; nSolarSystems++) {
			systems.add(Traders.solarSystemGenerator.getSolarSystem());
		}
		SolarSystemDataAccess.insert(systems);
		systems = SolarSystemDataAccess.all();
		List<SolarSystemsDistance> distances = new ArrayList<>();
		ListIterator<SolarSystem> it;
		SolarSystem system2;
		int i=0;
		for (SolarSystem system : systems) {
			it = systems.listIterator(i+1);
			while (it.hasNext()) {
				system2 = it.next();
				distances.add(new SolarSystemsDistance(system, system2, system.distance(system2)));
			}
			i++;
		}
		SolarSystemsDistanceDataAccess.insert(distances);
	}
	
	public void createPlanets() throws SQLException {
		List<Planet> planets = new ArrayList<>();
		List<SolarSystem> systems = SolarSystemDataAccess.all();
		for (SolarSystem system : systems) {
			// Number of planets
			long nPlanets = Traders.planetGenerator.getNumberOfPlanets();
			boolean atLeastOnePlanet=false;
			double distance = 0;
			for (long nPlanet=1; nPlanet<=nPlanets; nPlanet++) {
				distance = Traders.planetGenerator.getNextDistance(distance, (int)nPlanet);
				// Has trade?
				if ((atLeastOnePlanet==false && nPlanet==nPlanets) || current().nextDouble()<TradersConfig.current.tradeInPlanetProbability) {
					Planet planet = Traders.planetGenerator.getPlanet(system, (int)nPlanet, distance);
					planets.add(planet);
					atLeastOnePlanet=true;
				}
			}
		}
		PlanetDataAccess.insert(planets);
	}
	
	public void createProducts() throws SQLException {
		List<Product> products = new ArrayList<>();
		for (int nProducts=0; nProducts<TradersConfig.current.nProducts; nProducts++) {
			products.add(Traders.productGenerator.getProduct());
		}
		ProductDataAccess.insert(products);
	}
		
	private ShipOwner createShipOwner(Ship ship, Trader trader, LocalDate date) throws SQLException {
		String acquisitionCause = ShipOwner.acquisitionCauses[current().nextInt(
				ShipOwner.acquisitionCauses.length)];
		ShipOwner shipOwner = new ShipOwner(ship, trader, date, acquisitionCause);
		if (acquisitionCause.equals("Bought"))
			shipOwner.acquisitionPrice=ship.getBasePrice()*current().nextDouble(0.8, 1.2);
		return shipOwner;
	}
	
	private void initialShipAssignation(List<Trader> traders) throws SQLException {
		Ship ship;
		List<ShipOwner> shipOwners = new ArrayList<>();
		// Each trader must have a cargo ship at the beginning
		List<Ship> ships = ShipDataAccess.byType("Cargo ship");
		Collections.shuffle(ships);
		Iterator<Ship> itCargoShips = ships.iterator();
		for (Trader trader : traders) {
			ship = itCargoShips.next();
			shipOwners.add(createShipOwner(ship, trader, trader.startingDate));
		}
		ShipOwnerDataAccess.insert(shipOwners);
	}
	
	private Stay createStay(Trader trader, Planet planet, LocalDate startDate, LocalDate endDate, List<Stay> stays) 
			throws SQLException {
		Stay stay = new Stay();
		stay.startDate=startDate;
		stay.planet=planet;
		stay.trader=trader;
		stay.endDate=endDate;
		stays.add(stay);
		return stay;
	}
	
	private Stay initialStay(Trader trader, List<Stay> stays) throws SQLException {
		return createStay(trader, PlanetDataAccess.getRandom(), trader.startingDate, 
				trader.startingDate.plusDays(current().nextLong(1, TradersConfig.current.maxStayDuration)), stays);
	}
	
	private Stay nextStay(Stay lastStay, List<Stay> stays) throws SQLException {
		LocalDate travelStartDate = lastStay.endDate;
		Planet origin = lastStay.planet;
		Trader trader = lastStay.trader;
		PriceChange priceChange;
		Product product;
		do {
			product = ProductDataAccess.getRandom();
			priceChange = PriceChangeDataAccess.getBestPrice(product, travelStartDate);
		} while (priceChange == null);
		Planet destination = priceChange.planet;
		SolarSystem originSystem=SolarSystemDataAccess.getByPlanet(origin);
		SolarSystem destinationSystem=SolarSystemDataAccess.getByPlanet(destination);
		double distance = SolarSystemsDistanceDataAccess.distance(originSystem, destinationSystem);
		long travelDays=Math.round(distance*365*2*current().nextDouble(0.9,1.1))+
				current().nextLong(1, 100);
		LocalDate startDate = travelStartDate.plusDays(travelDays);
		LocalDate endDate = startDate.plusDays(current().nextLong(1, TradersConfig.current.maxStayDuration));
		Stay stay = createStay(trader, destination, startDate, endDate, stays);
		return stay;
	}
	
	private Trade sell(Stay stay, Trade lastBuy, LocalDate date, List<Trade> trades) throws SQLException {
		Trade trade = null;
		if (lastBuy!=null) {
			// Total ship capacity
			int capacity=0;
			List<Ship> ships = ShipDataAccess.ownedBy(stay.trader, date);
			for (Ship ship : ships) {
				capacity+=ship.capacity;
			}
			trade = new Trade();
			trade.dateTime=date.atTime(current().nextInt(0, 24), current().nextInt(0, 60));
			trade.stay=stay;
			trade.type=Trade.Type.Sell;
			Collections.shuffle(lastBuy.lines);
			for (TradeLine lastLine : lastBuy.lines) {
				TradeLine line = new TradeLine();
				line.product=lastLine.product;
				line.quantity=Math.min(lastLine.quantity, capacity);
				PriceChange priceChange = PriceChangeDataAccess.getLastChange(line.product, stay.planet, date);
				if (priceChange==null) { // Product not available, it's probably expensive
					line.unitPrice = current().nextDouble(
							PriceChangeDataAccess.averagePrice(line.product, date), 
							PriceChangeDataAccess.maxPrice(line.product, date)*2);
				} else { // Product available
					line.unitPrice = priceChange.newPrice*current().nextDouble(0.9, 1.1);
				}
				trade.total+=line.quantity*line.unitPrice;
				capacity-=line.quantity;
				if (line.quantity>0)
					trade.lines.add(line);
			}
			if (trade.lines.isEmpty())
				trade=null;
			else
				trades.add(trade);
		}
		return trade;
	}
	
	private Trade buy(Stay stay, LocalDate date, List<Trade> trades) throws SQLException {
		// Total ship capacity
		int capacity=0;
		List<Ship> ships = ShipDataAccess.ownedBy(stay.trader, date);
		for (Ship ship : ships) {
			capacity+=ship.capacity;
		}
		// Available products and prices
		List<Product> products = ProductDataAccess.all();
		List<PriceChange> lastPrices = new ArrayList<>();
		PriceChange lastPrice;
		for (Product product : products) {
			lastPrice = PriceChangeDataAccess.getLastChange(product, stay.planet, date);
			if (lastPrice!=null) {
				lastPrices.add(lastPrice);
			}
		}
		// Average prices
		List<PriceAndAverage> avgPrices = new ArrayList<>();
		for (PriceChange price : lastPrices) {
			avgPrices.add(new PriceAndAverage(price, PriceChangeDataAccess.averagePrice(price.product, date)));
		}
		Collections.sort(avgPrices);
		avgPrices.removeIf((avgPrice)->avgPrice.getRatio()>1);
		// Create trade
		Trade trade = new Trade();
		trade.dateTime=date.atTime(current().nextInt(0, 24), current().nextInt(0, 60));
		trade.stay=stay;
		trade.type=Trade.Type.Buy;
		int nProducts = avgPrices.size()>0?current().nextInt(1, Math.min(5, avgPrices.size()+1)):0;
		int nProduct=0;
		Iterator<PriceAndAverage> it = avgPrices.iterator();
		while (nProduct<nProducts && capacity>0) {
			PriceAndAverage p = it.next();
			TradeLine line = new TradeLine();
			line.product=p.price.product;
			line.quantity=(nProduct==nProducts-1?capacity:current().nextInt(1, capacity+1));
			line.unitPrice=p.price.newPrice*current().nextDouble(0.9, 1.1);
			trade.total+=line.quantity*line.unitPrice;
			capacity-=line.quantity;
			trade.lines.add(line);
			nProduct++;
		}
		if (trade.lines.isEmpty())
			trade = null;
		else
			trades.add(trade);
		return trade;
	}
	
	private class PriceAndAverage implements Comparable<PriceAndAverage> {
		PriceChange price;
		double average;
		
		PriceAndAverage(PriceChange price, double average) {
			this.price = price;
			this.average = average;
		}

		@Override
		public int compareTo(PriceAndAverage other) {
			return getRatio() < other.getRatio() ? 1 :
				(getRatio() > other.getRatio() ? -1 : 0);
		}
		
		double getRatio() {
			return price.newPrice/average;
		}
	}
	
	public static LocalDate randomDateBetween(LocalDate startDate, LocalDate endDate) {
		return startDate.plusDays(current().nextLong(startDate.until(endDate, ChronoUnit.DAYS)));
	}
	
	private void initTraders(List<Trader> traders) throws SQLException {
		// Activity span for each trader
		for (Trader trader : traders) {
			trader.startingDate = TradersConfig.current.startingDate.plusDays(current().nextLong(365*100));
			trader.endingDate = trader.startingDate.plusDays(current().nextLong(365*50, 365*100));
		}
		initialShipAssignation(traders);
	}
	
	private void traderStory(Trader trader) throws SQLException {
		List<Trade> trades = new ArrayList<>();
		List<Stay> stays = new ArrayList<>();
		Stay lastStay;
		Stay stay = initialStay(trader, stays);
		LocalDate date = randomDateBetween(stay.startDate, stay.endDate);
		Trade trade = buy(stay, date, trades);
		while (stay.endDate.isBefore(trader.endingDate) && stay.endDate.isBefore(TradersConfig.current.endingDate)) {
			// Travel
			lastStay = stay;
			stay = nextStay(lastStay, stays);
			// Lost ships during travel?
			while (current().nextDouble() < TradersConfig.current.shipLossProbability) {
				lossShip(trader, randomDateBetween(lastStay.endDate, stay.startDate));
			}
			// Sell products
			date = randomDateBetween(stay.startDate, stay.endDate);
			sell(stay, trade, date, trades);
			// Buy ships during stay?
			while (current().nextDouble() < TradersConfig.current.shipBuyProbability) {
				adquireShip(trader, randomDateBetween(stay.startDate, stay.endDate));
			}
			// Buy products
			date = randomDateBetween(date, stay.endDate);
			trade = buy(stay, date, trades);
		}
		traderEnd(trader, stay, trade, trades, stays);
		StayDataAccess.insert(stays);
		TradeDataAccess.insert(trades);
	}
	
	public void createStory() throws SQLException {
		System.out.println("Ships");
		createShips();
		System.out.println("Traders");
		createTraders();
		System.out.println("SolarSystems");
		createSolarSystems();
		System.out.println("Planets");
		createPlanets();
		System.out.println("Products");
		createProducts();
		System.out.println("Prices");
		(new PriceChangesSaver(TradersConfig.current.endingDate)).allPrices();
		System.out.println("Init traders");
		List<Trader> traders = TraderDataAccess.all();
		initTraders(traders);
		System.out.println("Travels");
		// Some history for each trader
		for (Trader trader : traders) {
			System.out.println("  Trader: "+trader.nickname);
			traderStory(trader);
		}
	}
	
	private void adquireShip(Trader trader, LocalDate date) throws SQLException {
		Ship ship;
		List<Ship> ships = ShipDataAccess.available(date);
		if (!ships.isEmpty()) {
			ship = ships.get(current().nextInt(ships.size()));
			ShipOwner shipOwner = createShipOwner(ship, trader, date);
			ShipOwnerDataAccess.insert(shipOwner);
		}
	}
	
	private void lossShip(Trader trader, LocalDate date) throws SQLException {
		Ship ship;
		ShipOwner shipOwner;
		List<Ship> ships = ShipDataAccess.ownedBy(trader, date);
		if (ships.size()>1) {
			ship = ships.get(current().nextInt(ships.size()));
			shipOwner = ShipOwnerDataAccess.byShipTraderDate(ship, trader, date);
			shipOwner.lostCause = ShipOwner.lostCauses[current().nextInt(
					ShipOwner.lostCauses.length)];
			shipOwner.lostDate = date;
			if (shipOwner.lostCause.equals("Sold"))
				shipOwner.lostBenefit=ship.getBasePrice()*current().nextDouble(0.8, 1.2);
			ShipOwnerDataAccess.update(shipOwner);
		}
	}
	
	private void traderEnd(Trader trader, Stay lastStay, Trade trade, List<Trade> trades, List<Stay> stays) throws SQLException {
		List<Ship> ships;
		// At the end, the trader may retire or die
		if (current().nextDouble()<TradersConfig.current.traderDieProbability) { //die
			String lostCause;
			if (current().nextBoolean()) { // in battle
				trader.deceaseDate=lastStay.endDate.plusDays(current().nextLong(1, 365*4));
				lostCause="Destroyed";
			} else { // in bed 
				trader.deceaseDate=lastStay.endDate;
				lostCause="Owner deceased";
			}
			TraderDataAccess.update(trader);
			ships = ShipDataAccess.ownedBy(trader, trader.deceaseDate);
			for (Ship s : ships) {
				endLossShip(s, trader, lostCause, trader.deceaseDate, 0);
			}
		} else { //retire
			// One last stay, without buying anything
			Stay stay = nextStay(lastStay, stays);
			// Lost ships during travel?
			while (current().nextDouble() < TradersConfig.current.shipLossProbability) {
				lossShip(trader, randomDateBetween(lastStay.endDate, stay.startDate));
			}
			// Sell products
			LocalDate date = randomDateBetween(stay.startDate, stay.endDate);
			sell(stay, trade, date, trades);
			ships = ShipDataAccess.ownedBy(trader, stay.endDate);
			for (Ship s : ships) {
				// Sell all ships
				endLossShip(s, trader, "Sold", stay.endDate, s.getBasePrice()*current().nextDouble(0.8, 1.2));
			}
		}
	}
	
	private void endLossShip(Ship ship, Trader trader, String lostCause, LocalDate lossDate, double lostBenefit) throws SQLException {
		ShipOwner shipOwner = ShipOwnerDataAccess.byShipTraderDate(ship, trader, lossDate);
		shipOwner.lostCause = lostCause;
		shipOwner.lostDate = lossDate;
		shipOwner.lostBenefit=lostBenefit;
		ShipOwnerDataAccess.update(shipOwner);
	}
	
}
