package com.violentbits.tradersdb.traders;

import static java.util.concurrent.ThreadLocalRandom.current;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import com.violentbits.tradersdb.dao.ProductDataAccess;
import com.violentbits.tradersdb.dao.PriceChangeDataAccess;
import com.violentbits.tradersdb.dao.SolarSystemDataAccess;
import com.violentbits.tradersdb.entities.Product;
import com.violentbits.tradersdb.entities.Planet;
import com.violentbits.tradersdb.entities.PriceChange;
import com.violentbits.tradersdb.entities.SolarSystem;

public class PriceChangesSaver {
	public final LocalDate endingDate;
	private List<Product> products;
	private SolarSystem system;
	private List<PriceChange> priceChanges = new ArrayList<>();
	
	public PriceChangesSaver(LocalDate endingDate) {
		this.endingDate = endingDate;
	}
	
	private void systemPrices() throws SQLException {
		int nProducts = current().nextInt(TradersConfig.current.minProductsPerPlanet,
				Math.min(TradersConfig.current.maxProductsPerPlanet, products.size()));
		Collections.shuffle(products);
		Iterator<Product> itProduct = products.iterator();
		priceChanges.clear();
		for (int nProduct=0; nProduct<nProducts; nProduct++) {
			Product product = itProduct.next();
			productPrices(product);
		}
		PriceChangeDataAccess.insert(priceChanges);
	}
	
	private void productPrices(Product product) throws SQLException {
		double price = product.basePrice*current().nextDouble(0.5, 1.5);
		LocalDate date = TradersConfig.current.startingDate;
		Collections.shuffle(system.getPlanets());
		int nPlanets = current().nextInt(1, system.getPlanets().size()+1);
		while (date.isBefore(endingDate)) {	
			productPricesInSystem(product, date, price, nPlanets);
			price*=current().nextDouble(0.7, 1.35);
			date = date.plusDays(current().nextLong(TradersConfig.current.minDaysWithoutPriceChanges, TradersConfig.current.maxDaysWithoutPriceChanges));
		}
	}
	
	private void productPricesInSystem(Product product, LocalDate date, double price, int nPlanets) 
			throws SQLException {
		PriceChange priceChange;
		Planet planet;
		Iterator<Planet> itPlanet = system.getPlanets().iterator();
		for (int nPlanet=0; nPlanet<nPlanets; nPlanet++) {
			planet = itPlanet.next();
			priceChange = new PriceChange();
			priceChange.date = date.plusDays(current().nextLong(0, TradersConfig.current.minDaysWithoutPriceChanges));
			priceChange.product = product;
			priceChange.planet = planet;
			priceChange.newPrice = price*current().nextDouble(0.9, 1.1);
			priceChanges.add(priceChange);
		}
	}
	
	public void allPrices() throws SQLException {
		List<SolarSystem> systems = SolarSystemDataAccess.all();
		products = ProductDataAccess.all();
		for (SolarSystem system : systems) {
			this.system = system;
			systemPrices();
		}
	}
}
