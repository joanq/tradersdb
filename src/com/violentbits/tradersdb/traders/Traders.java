package com.violentbits.tradersdb.traders;

import java.io.IOException;

import com.violentbits.tradersdb.generators.PlanetGenerator;
import com.violentbits.tradersdb.generators.ProductGenerator;
import com.violentbits.tradersdb.generators.ShipGenerator;
import com.violentbits.tradersdb.generators.ShipTypeGenerator;
import com.violentbits.tradersdb.generators.SolarSystemGenerator;
import com.violentbits.tradersdb.generators.TraderGenerator;

public class Traders {
	public static ShipTypeGenerator shipTypeGenerator;
	public static ShipGenerator shipGenerator;
	public static TraderGenerator traderGenerator;
	public static SolarSystemGenerator solarSystemGenerator;
	public static PlanetGenerator planetGenerator;
	public static ProductGenerator productGenerator;
	
	private Traders() {
		;
	}
	
	static void init() throws IOException {
		shipTypeGenerator = new ShipTypeGenerator();
		shipGenerator = new ShipGenerator();
		traderGenerator = new TraderGenerator();
		solarSystemGenerator = new SolarSystemGenerator();
		planetGenerator = new PlanetGenerator();
		productGenerator = new ProductGenerator();
	}
	

}
