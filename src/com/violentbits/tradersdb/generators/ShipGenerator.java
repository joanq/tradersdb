package com.violentbits.tradersdb.generators;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import com.violentbits.dbutils.csv.CSVReader;
import com.violentbits.dbutils.storage.OrderedDataStorage;
import com.violentbits.tradersdb.entities.Ship;
import com.violentbits.tradersdb.entities.ShipType;
import com.violentbits.tradersdb.traders.Traders;
import com.violentbits.tradersdb.traders.TradersConfig;

public class ShipGenerator {
	private OrderedDataStorage<String> allNamesDataStorage;
	
	public ShipGenerator() throws IOException {
		List<String> shipNames = CSVReader.readOne(TradersConfig.current.shipsFile);
		allNamesDataStorage = new OrderedDataStorage<String>(shipNames);
	}
	
	public Ship getShip() {
		String name = allNamesDataStorage.get();
		ShipType shipType = Traders.shipTypeGenerator.getShipType();
		if (shipType.prefixes!=null && shipType.prefixes.length>0 &&
				ThreadLocalRandom.current().nextInt(100)<=30) {
			name=shipType.prefixes[ThreadLocalRandom.current().nextInt(shipType.prefixes.length)]+" "+name;
		}
		int capacity = shipType.getRandomCapacity();
		return new Ship(name, shipType, capacity);
	}
}
