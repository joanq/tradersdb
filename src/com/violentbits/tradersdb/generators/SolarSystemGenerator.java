package com.violentbits.tradersdb.generators;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import com.violentbits.dbutils.csv.CSVReader;
import com.violentbits.dbutils.storage.IntegerDataStorage;
import com.violentbits.dbutils.storage.OrderedDataStorage;
import com.violentbits.tradersdb.dao.SolarSystemDataAccess;
import com.violentbits.tradersdb.entities.SolarSystem;
import com.violentbits.tradersdb.traders.TradersConfig;

public class SolarSystemGenerator {
	private OrderedDataStorage<String> solarsystemNamesDataStorage;
	private IntegerDataStorage digitDataStorage = new IntegerDataStorage(0, 10);
	private IntegerDataStorage charDataStorage = new IntegerDataStorage('A', 'Z'+1);
	
	public SolarSystemGenerator() throws IOException {
		List<String> solarsystemNamesList = CSVReader.readOne(TradersConfig.current.solarsystemsFile);
		
		solarsystemNamesDataStorage = new OrderedDataStorage<String>(solarsystemNamesList);
		
	}
	
	public SolarSystem getSolarSystem() throws SQLException {
		String name = solarsystemNamesDataStorage.get();
		String code;
		
		do {
			code = ""+Character.toUpperCase(name.charAt(0))+Character.toUpperCase(name.charAt(1))+
					digitDataStorage.getString()+digitDataStorage.getString()+
					(char)(int)charDataStorage.get()+(char)(int)charDataStorage.get();
		} while (SolarSystemDataAccess.getByCode(code)!=null);
		
		return new SolarSystem(code, name);
	}
}
