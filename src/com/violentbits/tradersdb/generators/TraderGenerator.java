package com.violentbits.tradersdb.generators;

import java.io.IOException;
import java.util.List;

import com.violentbits.dbutils.csv.CSVReader;
import com.violentbits.dbutils.storage.OrderedDataStorage;
import com.violentbits.tradersdb.entities.Trader;
import com.violentbits.tradersdb.traders.TradersConfig;

public class TraderGenerator {
	private OrderedDataStorage<String> firstnamesDataStorage;
	private OrderedDataStorage<String> lastnamesDataStorage;
	private OrderedDataStorage<String> nicknamesDataStorage;
	
	public TraderGenerator() throws IOException {
		List<String> firstnamesList = CSVReader.readOne(TradersConfig.current.firstnamesFile);
		List<String> lastnamesList = CSVReader.readOne(TradersConfig.current.lastnamesFile);
		List<String> nicknamesList = CSVReader.readOne(TradersConfig.current.nicknamesFile);
		
		firstnamesDataStorage = new OrderedDataStorage<String>(firstnamesList);
		lastnamesDataStorage = new OrderedDataStorage<String>(lastnamesList);
		nicknamesDataStorage = new OrderedDataStorage<String>(nicknamesList);
	}
	
	public Trader getTrader() {
		String firstname = firstnamesDataStorage.get();
		String lastname = lastnamesDataStorage.get();
		String nickname = nicknamesDataStorage.get();
		return new Trader(nickname, firstname, lastname);
	}
}
