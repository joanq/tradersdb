package com.violentbits.tradersdb.generators;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.violentbits.dbutils.csv.CSVReader;
import com.violentbits.dbutils.storage.WeightedDataStorage;
import com.violentbits.tradersdb.entities.ShipType;
import com.violentbits.tradersdb.traders.TradersConfig;

public class ShipTypeGenerator {
	private WeightedDataStorage<ShipType> shipTypesStorage;
	
	public ShipTypeGenerator() throws IOException {
		createStorage();
	}
	
	private void createStorage() throws IOException {
		List<String[]> shipTypesList = CSVReader.read(TradersConfig.current.shipTypesFile);
		List<ShipType> shipTypes = new ArrayList<>();
		List<Double> freq = new ArrayList<>();
		for (String[] strArray : shipTypesList) {
			ShipType shipType = new ShipType(
					strArray[0],
					Integer.parseInt(strArray[2]),
					Integer.parseInt(strArray[3]),
					strArray.length>4?strArray[4].split(":"):null);
			shipTypes.add(shipType);
			freq.add(Double.parseDouble(strArray[1]));
		}
		shipTypesStorage = new WeightedDataStorage<>(shipTypes, freq);
	}
	
	public ShipType getShipType() {
		return shipTypesStorage.get();
	}
	
	public ShipType byName(String shipTypeName) {
		List<ShipType> allShipTypes = shipTypesStorage.getAll();
		ShipType result=null;
		for (ShipType shipType : allShipTypes) {
			if (shipType.name.equals(shipTypeName)) {
				result = shipType;
			}
		}
		return result;
	}
}
