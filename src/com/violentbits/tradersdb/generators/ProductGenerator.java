package com.violentbits.tradersdb.generators;

import java.io.IOException;
import java.util.List;

import com.violentbits.dbutils.csv.CSVReader;
import com.violentbits.dbutils.storage.OrderedDataStorage;
import com.violentbits.tradersdb.entities.Product;
import com.violentbits.tradersdb.traders.TradersConfig;

public class ProductGenerator {
	private OrderedDataStorage<String> nameDataStorage;
	
	public ProductGenerator() throws IOException {
		List<String> productNames = CSVReader.readOne(TradersConfig.current.productsFile);
		nameDataStorage = new OrderedDataStorage<>(productNames);
	}
	
	public Product getProduct() {
		return new Product(nameDataStorage.get());
	}
}
