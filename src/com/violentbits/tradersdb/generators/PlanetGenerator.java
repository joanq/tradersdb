package com.violentbits.tradersdb.generators;

import java.io.IOException;
import java.util.List;

import com.violentbits.dbutils.csv.CSVReader;
import com.violentbits.dbutils.storage.NormalDataStorage;
import com.violentbits.dbutils.storage.OrderedDataStorage;
import com.violentbits.tradersdb.entities.Planet;
import com.violentbits.tradersdb.entities.SolarSystem;
import com.violentbits.tradersdb.traders.TradersConfig;

public class PlanetGenerator {
	private OrderedDataStorage<String> planetNamesDataStorage;
	private NormalDataStorage distanceStorage = new NormalDataStorage(0.6, 0.4, 0.05, 5);
	private NormalDataStorage numberOfPlanets = new NormalDataStorage(
			TradersConfig.current.avgPlanetsPerSystem,
			TradersConfig.current.stddevPlanetsPerSystem,
			TradersConfig.current.minPlanetsPerSystem,
			TradersConfig.current.maxPlanetsPerSystem
	);
	
	public PlanetGenerator() throws IOException {
		List<String> planetNamesList = CSVReader.readOne(TradersConfig.current.planetsFile);
		planetNamesDataStorage = new OrderedDataStorage<String>(planetNamesList);
	}
	
	public double getNextDistance(double lastDistance, int pos) {
		return lastDistance + distanceStorage.get()*pos;
	}

	public Planet getPlanet(SolarSystem system, int pos, double distance) {
		String name = planetNamesDataStorage.get();
		return new Planet(name, pos, system, distance);
	}
	
	public long getNumberOfPlanets() {
		return numberOfPlanets.getLong();
	}
}
